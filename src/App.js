import Body from "./Smart_Phone_Shop/Body";
import Cart from "./Smart_Phone_Shop/Cart";

function App() {
  return (
    <div className="App">
      <Body />
      <Cart />
    </div>
  );
}

export default App;

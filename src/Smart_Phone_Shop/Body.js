import React, { useState, useEffect } from "react";
import Header from "./Header";
import ProductList from "./ProductList";
import data_list from "./phones_data.json";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import { add, increment, selectCart } from "../features/cart/cartSlice";
export default function Body() {
  const cart = useSelector(selectCart);
  const [data, setData] = useState(data_list);
  const dispatch = useDispatch();
  const handleAddItem = (e) => {
    const id = e.target.parentNode.parentNode.id;
    let item_add = { ...data_list[id] };
    item_add.quantity = 1;
    console.log(" item_add: ", item_add);
    let index = cart.findIndex((item) => item.name === item_add.name);
    if (index === -1) {
      dispatch(add(item_add));
    } else {
      dispatch(increment(index));
    }
  };
  const handleShowIphone = () => {
    const iphone_data = data_list.filter((phone) => {
      return phone.type === "Iphone";
    });
    setData(iphone_data);
  };
  const handleShowSamsung = () => {
    const samsung_data = data_list.filter((phone) => {
      return phone.type === "Samsung";
    });
    setData(samsung_data);
  };
  const handleBackToAll = () => {
    setData(data_list);
  };
  useEffect(() => {
    console.log("yes");
  }, []);
  return (
    <div>
      <Header
        handleShowIphone={handleShowIphone}
        handleShowSamsung={handleShowSamsung}
        handleBackToAll={handleBackToAll}
      />
      <ProductList data={data} handleAddItem={handleAddItem} />
    </div>
  );
}

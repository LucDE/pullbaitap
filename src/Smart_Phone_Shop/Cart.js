//có thể chuyển phần CartItem vào đây
import React from "react";
import { useSelector } from "react-redux";
import { selectCart } from "../features/cart/cartSlice";
import CartItem from "./CartItem";

export default function Cart() {
  const cart = useSelector(selectCart);
  const handleHideCart = () => {
    document.querySelector("#cart").classList.add("d-none");
  };
  const getSum = (total, num) => {
    return total + num;
  };
  const total_purchase_item = cart
    .map((item) => item.price * item.quantity)
    .reduce(getSum, 0);
  return (
    <div
      className="cart d-none py-5 px-3 animate__fadeInDownBig animate__animated"
      id="cart"
    >
      {cart.length > 0 ? (
        <table className="text-white w-100" id="table_cart">
          <thead style={{ textAlign: "center" }}>
            <tr>
              <td className="text-left">Hình ảnh</td>
              <td className="text-left">Tên sản phẩm</td>
              <td className="text-left">Số lượng</td>
              <td>Giá tiền</td>
              <td className="text-right">Thành tiền</td>
              <td className="text-right">Thao tác</td>
            </tr>
          </thead>
          <tbody>
            <CartItem />
            <tr>
              <td>Tổng tiền:</td>
              <td></td>
              <td></td>
              <td></td>
              <td className="text-center">{total_purchase_item + " $"}</td>
            </tr>
            <tr id="td_no_border">
              <td></td>
              <td></td>
              <td></td>
              <td>
                <button className="p-2 btn-success">Thanh toán</button>
              </td>
              <td>
                <button className="p-2 btn-danger">Hủy đơn</button>
              </td>
            </tr>
          </tbody>
        </table>
      ) : (
        <div>
          <p className="text-white">Mời bạn chọn sản phẩm!</p>
        </div>
      )}
      <button
        className="btn text-danger bg-white"
        id="btn_close"
        onClick={handleHideCart}
      >
        X
      </button>
    </div>
  );
}
/* <table className="text-white w-100" id="table_cart">
<thead style={{ textAlign: "center" }}>
  <tr>
    <td>Hình ảnh</td>
    <td>Tên sản phẩm</td>
    <td className="text-left">Số lượng</td>
    <td>Giá tiền</td>
    <td className="text-right">Thành tiền</td>
    <td className="text-right">Thao tác</td>
  </tr>
</thead>
<tbody>
  <CartItem />
  <tr>
    <td>Tổng tiền:</td>
    <td></td>
    <td></td>
    <td></td>
    <td className="text-center">{total_purchase_item}</td>
  </tr>
  <tr id="td_no_border">
    <td></td>
    <td></td>
    <td></td>
    <td>
      <button className="p-2 btn-success">Thanh toán</button>
    </td>
    <td>
      <button className="p-2 btn-danger">Hủy đơn</button>
    </td>
  </tr>
</tbody>
</table> */

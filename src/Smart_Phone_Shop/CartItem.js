import React from "react";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import {
  decrement,
  increment,
  remove,
  selectCart,
} from "../features/cart/cartSlice";

export default function CartItem(props) {
  const dispatch = useDispatch();
  const cart = useSelector(selectCart);
  const handleDecreaseItem = (e) => {
    // let index = cart.find()
    let index = cart.findIndex((item) => item.name === e.target.dataset.name);
    dispatch(decrement(index));
  };
  const handleIncreaseItem = (e) => {
    //set item,dispatch
    let index = cart.findIndex((item) => item.name === e.target.dataset.name);
    dispatch(increment(index));
  };
  const handleDeleteItem = (e) => {
    //set item,dispatch
    let index = cart.findIndex((item) => item.name === e.target.dataset.name);
    dispatch(remove(index));
  };

  return cart.map((item, index) => {
    return (
      <tr key={index}>
        <td className="p-2" style={{ width: "20%" }}>
          <img width="80px" src={item.img} alt={item.name} />
        </td>
        <td style={{ width: "35%" }}>{item.name}</td>
        <td>
          <div className="d-flex justify-content-center align-items-center">
            <button
              data-name={item.name}
              aria-label="Decrement value"
              onClick={handleDecreaseItem}
            >
              -
            </button>
            <p id="showCount" className="mx-3 mb-0">
              {item.quantity}
            </p>
            <button
              data-name={item.name}
              aria-label="Increment value"
              onClick={handleIncreaseItem}
            >
              +
            </button>
          </div>
        </td>
        <td className="text-center">{item.price}</td>
        <td className="text-right">{item.price * item.quantity}</td>
        <td className="text-right">
          <button data-name={item.name} onClick={handleDeleteItem}>
            <i className="fa fa-trash"></i>
          </button>
        </td>
      </tr>
    );
  });
}

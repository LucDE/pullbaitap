import React from "react";
import { useSelector } from "react-redux";
import { selectCart } from "../features/cart/cartSlice";
export default function Header(props) {
  const cart = useSelector(selectCart);
  const handleShowCart = () => {
    document.querySelector("#cart").classList.remove("d-none");
  };
  const getSum = (total, num) => {
    return total + num;
  };
  const total_purchase_item = cart
    .map((item) => item.quantity)
    .reduce(getSum, 0);
  return (
    <div>
      <header>
        <div className="container d-flex p-3 align-items-center">
          <div>
            <button className="pr-5 text-dark" onClick={props.handleShowIphone}>
              <i className="fab fa-apple" />
            </button>
          </div>
          <div>
            <button onClick={props.handleShowSamsung}>
              <img
                width="100px"
                src="https://cdn-icons-png.flaticon.com/512/882/882849.png"
                alt="samsung"
              />
            </button>
          </div>
          <div className="back" id="back">
            <button>
              <i
                className="fa-solid fa-arrow-left text-dark"
                onClick={props.handleBackToAll}
              />
            </button>
          </div>
        </div>
        <div className="cart_container">
          <button onClick={handleShowCart}>
            <i className="fa fa-shopping-cart" />
          </button>
          <div className="item_info">
            <span id="item_alert">{total_purchase_item}</span>
          </div>
        </div>
      </header>
    </div>
  );
}

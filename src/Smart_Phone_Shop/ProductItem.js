import React from "react";

export default function ProductItem(props) {
  const data_list = props.data_list;
  return data_list.map((phone, index) => {
    return (
      <div key={phone.name} className="col-3 p-3" id={index}>
        <div className="column_container p-2">
          <img src={phone.img} alt={phone.name} />
          <p>{phone.name}</p>
          <p>{phone.desc}</p>
          <p>{phone.screen}</p>
          <p>{phone.backCamera}</p>
          <p>{phone.frontCamera}</p>
          <p>{phone.price}</p>
          <button className="btn" id="add_button" onClick={props.handleAddItem}>
            +
          </button>
        </div>
      </div>
    );
  });
}

import React from "react";
import ProductItem from "./ProductItem";

export default function ProductList(props) {
  const data = props.data;

  return (
    <div className="container">
      <div className="row" id="row_render">
        <ProductItem data_list={data} handleAddItem={props.handleAddItem} />
      </div>
    </div>
  );
}

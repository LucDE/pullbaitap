import { createSlice } from "@reduxjs/toolkit";

let cart = JSON.parse(localStorage.getItem("cart"));
let initialState = [];
if (cart) {
  initialState = cart;
}

export const cartSlice = createSlice({
  name: "cart",
  initialState,
  reducers: {
    add: (state, action) => {
      state.push(action.payload);
      localStorage.setItem("cart", JSON.stringify(state));
    },
    increment: (state, action) => {
      state[action.payload].quantity = state[action.payload].quantity + 1;
      localStorage.setItem("cart", JSON.stringify(state));
    },
    decrement: (state, action) => {
      let index = action.payload;
      if (state[index].quantity === 1) {
        return;
      }
      state[index].quantity = state[index].quantity - 1;
      localStorage.setItem("cart", JSON.stringify(state));
    },
    remove: (state, action) => {
      state.splice(action.payload, 1);
      localStorage.setItem("cart", JSON.stringify(state));
    },
  },
});

//chỗ này cartSlice đã tạo sẵn các action objects để hàm dispatch sử dụng, chỉ cần khai báo các giá trị name,initialState, reducers bên trong hàm createSlice (hàm khởi tạo các lớp reducers function khác nhau)
export const { add, remove, increment, decrement } = cartSlice.actions;

// The function below is called a selector and allows us to select a value from
// the state. Selectors can also be defined inline where they're used instead of
// in the slice file. For example: `useSelector((state: RootState) => state.counter.value)`
export const selectCart = (state) => state.cart;
export default cartSlice.reducer;
